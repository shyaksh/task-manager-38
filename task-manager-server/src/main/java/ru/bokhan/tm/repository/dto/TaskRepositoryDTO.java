package ru.bokhan.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.dto.TaskDTO;

import java.util.List;

public interface TaskRepositoryDTO extends AbstractRepositoryDTO<TaskDTO> {

    TaskDTO findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    TaskDTO findByUserIdAndName(@NotNull final String userId, @NotNull final String name);

    List<TaskDTO> findByUserId(@NotNull final String userId);

}
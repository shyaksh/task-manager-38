package ru.bokhan.tm.repository.entity;

import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.entity.Session;

public interface SessionRepository extends AbstractRepository<Session> {

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserId(@NotNull final String userId);

}
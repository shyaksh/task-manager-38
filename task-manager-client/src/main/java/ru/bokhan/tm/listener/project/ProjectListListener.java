package ru.bokhan.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.api.ICurrentSessionService;
import ru.bokhan.tm.endpoint.ProjectDTO;
import ru.bokhan.tm.endpoint.ProjectEndpoint;
import ru.bokhan.tm.endpoint.SessionDTO;
import ru.bokhan.tm.event.ConsoleEvent;
import ru.bokhan.tm.exception.security.AccessDeniedException;
import ru.bokhan.tm.listener.AbstractListener;

import java.util.List;

@Component
public final class ProjectListListener extends AbstractListener {

    @Autowired
    @NotNull
    private ProjectEndpoint projectEndpoint;

    @Autowired
    @NotNull
    private ICurrentSessionService currentSessionService;

    @NotNull
    @Override
    public String command() {
        return "project-list";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list";
    }

    @Override
    @EventListener(condition = "@projectListListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @Nullable final SessionDTO session = currentSessionService.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[LIST PROJECTS]");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectAll(session);
        for (@Nullable final ProjectDTO project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

}

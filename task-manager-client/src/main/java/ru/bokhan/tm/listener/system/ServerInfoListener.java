package ru.bokhan.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.endpoint.ServerInfoEndpoint;
import ru.bokhan.tm.event.ConsoleEvent;
import ru.bokhan.tm.listener.AbstractListener;

@Component
public final class ServerInfoListener extends AbstractListener {

    @NotNull
    @Autowired
    private ServerInfoEndpoint serverInfoEndpoint;

    @NotNull
    @Override
    public String command() {
        return "server-info";
    }

    @NotNull
    @Override
    public String argument() {
        return "-s";
    }

    @NotNull
    @Override
    public String description() {
        return "Show server host and port.";
    }

    @Override
    @EventListener(condition = "@serverInfoListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[SERVER INFO]");
        System.out.println("Server host: " + serverInfoEndpoint.getServerHost());
        System.out.println("Server port: " + serverInfoEndpoint.getServerPort());
    }

}
